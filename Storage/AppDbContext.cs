﻿using Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Storage
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Refueling> Refuelings { get; set; }
        public DbSet<Camera> Cameras { get; set; }
        public DbSet<PermissionVerificationResult> PermissionVerificationResults { get; set; }
        public DbSet<Recognition> Recognitions { get; set; }
        public DbSet<RegistrationPlatePhoto> RegistrationPlatePhotos { get; set; }
    }
}