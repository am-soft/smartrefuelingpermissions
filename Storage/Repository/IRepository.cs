﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Domain.Model;

namespace Storage.Repository
{
    public interface IRepository<TEntity>
        where TEntity : BaseEntity
    {
        IQueryable<TEntity> Query();
        Task<TEntity> Get(Guid id);
        Task Create(TEntity entity);
        Task Update(TEntity entity);
        Task Delete(TEntity entity);
    }
}