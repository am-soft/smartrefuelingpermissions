﻿using System;
using System.Collections.Generic;

namespace Domain.Model
{
    public class Refueling : BaseEntity
    {
        public DateTime DateTime { get; set; }
        public Guid DistributorId { get; set; }
        public IEnumerable<RegistrationPlatePhoto> RegistrationPlatePhotos { get; set; } = new List<RegistrationPlatePhoto>();
    }
}