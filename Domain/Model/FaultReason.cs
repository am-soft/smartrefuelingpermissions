﻿namespace Domain.Model
{
    public enum FaultReason
    {
        TimeoutExceeded,
        UnableToRecognize
    }
}