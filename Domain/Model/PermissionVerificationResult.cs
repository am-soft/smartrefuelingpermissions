﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Model
{
    public class PermissionVerificationResult : BaseEntity
    {
        public Recognition Recognition { get; set; }
        [ForeignKey("RecognitionId")]
        public Guid RecognitionId { get; set; }
        public bool AllowedToTank { get; set; }
    }
}