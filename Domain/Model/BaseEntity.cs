﻿using System;

namespace Domain.Model
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}