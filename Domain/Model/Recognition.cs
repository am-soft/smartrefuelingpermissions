﻿namespace Domain.Model
{
    public class Recognition : BaseEntity
    {
        public decimal Accuracy { get; set; }
        public string RegistrationPlateNumber { get; set; }
    }
}