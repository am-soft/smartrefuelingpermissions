﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Model
{
    public class RegistrationPlatePhoto : BaseEntity
    {
        public byte[] Photo { get; set; }

        [ForeignKey("CameraId")]
        public Guid CameraId { get; set; }
        public Camera Camera { get; set; }

        [ForeignKey("RefuelingId")]
        public Guid RefuelingId { get; set; }
        public Refueling Refueling { get; set; }

        [ForeignKey("RecognitionId")]
        public Guid RecognitionId { get; set; }
        public Recognition Recognition { get; set; }
        public DateTime DateTime { get; set; }
    }
}