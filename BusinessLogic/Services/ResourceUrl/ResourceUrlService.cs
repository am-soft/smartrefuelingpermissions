﻿using Domain.Model;

namespace BusinessLogic.Services.ResourceUrl
{
    public static class ResourceUrlService
    {
        public static string GetUrl(BaseEntity resource)
        {
            return $"/{GetResourcePath(resource)}/{resource.Id}";
        }

        private static string GetResourcePath(BaseEntity resource)
        {
            return resource.GetType().Name.ToLower();
        }
    }
}