﻿namespace BusinessLogic.Services.Verifications.Responses
{
    public class VerifyPermissionsResponse
    {
        public string MatchedLicensePlate { get; set; }
        public bool? Allowed { get; set; }
    }
}