﻿using System;
using BusinessLogic.Services.Verifications.Responses;
using MediatR;

namespace BusinessLogic.Services.Verifications.Requests
{
    public class VerifyPermissionsRequest : IRequest<VerifyPermissionsResponse>
    {
        public Guid RefuelingId { get; set; }
        public string[] AllowedLicensePlates { get; set; }
    }
}