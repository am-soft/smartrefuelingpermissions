﻿using System.Data;
using BusinessLogic.Services.Refuelings.Validators;
using BusinessLogic.Services.Verifications.Requests;
using FluentValidation;
using MediatR;

namespace BusinessLogic.Services.Verifications.Validators
{
    public class VerifyPermissionsRequestValidator : AbstractValidator<VerifyPermissionsRequest>
    {
        public VerifyPermissionsRequestValidator(IMediator mediator)
        {
            RuleFor(x => x.RefuelingId)
                .SetValidator(new RefuelingIdValidator(mediator));

            RuleFor(x => x.AllowedLicensePlates)
                .NotNull()
                .NotEmpty();

            RuleForEach(x => x.AllowedLicensePlates)
                .NotNull()
                .NotEmpty();
        }
    }
}