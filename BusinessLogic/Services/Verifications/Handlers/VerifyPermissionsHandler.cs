﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Notifications.Requests;
using BusinessLogic.Services.Verifications.Requests;
using BusinessLogic.Services.Verifications.Responses;
using Domain.Model;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Storage.Repository;

namespace BusinessLogic.Services.Verifications.Handlers
{
    public class VerifyPermissionsHandler : IRequestHandler<VerifyPermissionsRequest, VerifyPermissionsResponse>
    {
        private readonly IRepository<Refueling> _refuelingsRepository;
        private readonly IRepository<Recognition> _recognitionsRepository;
        private readonly IRepository<RegistrationPlatePhoto> _registrationPlatePhotosRepository;
        private readonly IMediator _mediator;

        public VerifyPermissionsHandler(IRepository<Refueling> refuelingsRepository, IRepository<Recognition> recognitionsRepository, IRepository<RegistrationPlatePhoto> registrationPlatePhotosRepository, IMediator mediator)
        {
            _refuelingsRepository = refuelingsRepository;
            _recognitionsRepository = recognitionsRepository;
            _registrationPlatePhotosRepository = registrationPlatePhotosRepository;
            _mediator = mediator;
        }

        public async Task<VerifyPermissionsResponse> Handle(VerifyPermissionsRequest request, CancellationToken cancellationToken)
        {
            var refueling = await _refuelingsRepository.Get(request.RefuelingId);

            var registrationPlatePhotos = await _registrationPlatePhotosRepository
                .Query()
                .Where(x => x.RefuelingId == request.RefuelingId && x.RecognitionId != null)
                .ToListAsync(cancellationToken: cancellationToken);

            if (!registrationPlatePhotos.Any())
                return new VerifyPermissionsResponse();

            var recognitionIds = registrationPlatePhotos
                .Select(x => x.RecognitionId);

            var recognitions = _recognitionsRepository
                .Query()
                .Where(x => recognitionIds.Contains(x.Id))
                .ToList();

            var bestRecognition = recognitions
                .OrderByDescending(x => x.Accuracy)
                .First();

            foreach (var licensePlate in request.AllowedLicensePlates)
            {
                if (bestRecognition.RegistrationPlateNumber.Replace("0", "O").ToLowerInvariant() ==
                    licensePlate.Replace("0", "O").ToLowerInvariant())
                    return new VerifyPermissionsResponse
                    {
                        Allowed = true,
                        MatchedLicensePlate = licensePlate
                    };
            }

            await _mediator.Send(new NotifyLicensePlateNotAllowedRequest { Refueling = refueling }, cancellationToken);

            return new VerifyPermissionsResponse();
        }
    }
}