﻿using System;
using Domain.Model;
using MediatR;

namespace BusinessLogic.Services.Recognitions.Requests
{
    public class RecognizeRequest : IRequest<Recognition>
    {
        public Guid RefuelingId { get; set; }
    }
}