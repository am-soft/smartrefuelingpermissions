﻿using System;
using System.Linq;
using BusinessLogic.Services.Recognitions.Requests;
using Domain.Model;
using FluentValidation;
using Storage.Repository;

namespace BusinessLogic.Services.Recognitions.Validators
{
    public class RecognizeValidator : AbstractValidator<RecognizeRequest>
    {
        private readonly IRepository<RegistrationPlatePhoto> _photosRepository;

        public RecognizeValidator(IRepository<RegistrationPlatePhoto> photosRepository)
        {
            _photosRepository = photosRepository;

            RuleFor(x => x.RefuelingId)
                .Must(HavePhotoAvailable)
                .WithMessage("No photos available for recognizing this refueling");
        }

        private bool HavePhotoAvailable(Guid refuelingId)
        {
            var refuelingPhotosCount = _photosRepository
                .Query()
                .Count(x => x.RefuelingId == refuelingId);

            return refuelingPhotosCount >= 1;
        }
    }
}