﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Notifications.Requests;
using BusinessLogic.Services.Recognitions.Requests;
using BusinessLogic.Services.Refuelings.Requests;
using Domain.Model;
using MediatR;
using openalprnet;
using Storage.Repository;

namespace BusinessLogic.Services.Recognitions.Handlers
{
    public class RecognizeHandler : IRequestHandler<RecognizeRequest, Recognition>
    {
        private readonly IMediator _mediator;
        private readonly IRepository<Recognition> _recognitionsRepository;
        private readonly IRepository<RegistrationPlatePhoto> _photosRepository;
        private readonly IRepository<Refueling> _refuelingsRepository;

        public RecognizeHandler(IMediator mediator, IRepository<Recognition> recognitionsRepository, IRepository<RegistrationPlatePhoto> photosRepository, IRepository<Refueling> refuelingsRepository)
        {
            _mediator = mediator;
            _recognitionsRepository = recognitionsRepository;
            _photosRepository = photosRepository;
            _refuelingsRepository = refuelingsRepository;
        }

        public async Task<Recognition> Handle(RecognizeRequest request, CancellationToken cancellationToken)
        {
            var refueling = await _refuelingsRepository.Get(request.RefuelingId);

            var photo = await _mediator.Send(new GetRefuelingPhotoToRecognizeRequest { RefuelingId = request.RefuelingId }, cancellationToken);
            if (photo == null)
                throw new ApplicationException("No photo available for recognition for this refueling");

            var alpr = new AlprNet("eu", "C://openalpr//openalpr.conf", "C://openalpr//runtime_data");
            if (!alpr.IsLoaded())
                throw new ApplicationException("Could not load recognition component");

            var results = alpr.Recognize(photo.Photo);
            var plates = results.Plates;

            if (!plates.Any())
            {
                await _mediator.Send(new NotifyUnableToRecognizeRequest { Refueling = refueling }, cancellationToken);
                return null;
            }

            var recognition = new Recognition
            {
                RegistrationPlateNumber = plates.First().BestPlate.Characters,
                Accuracy = (decimal)plates.First().BestPlate.OverallConfidence
            };

            await _recognitionsRepository.Create(recognition);

            photo.RecognitionId = recognition.Id;
            await _photosRepository.Update(photo);

            return recognition;
        }
    }
}