﻿using System;
using BusinessLogic.Responses;
using MediatR;

namespace BusinessLogic.Services.Cameras.Requests
{
    public class TakePhotoRequest : IRequest<BaseResourceResponse>
    {
        public Guid RefuelingId { get; set; }

        public byte[] File { get; set; }
    }
}