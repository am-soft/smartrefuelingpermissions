﻿using System;
using Domain.Model;
using MediatR;

namespace BusinessLogic.Services.Cameras.Requests
{
    public class GetPhotoRequest : IRequest<RegistrationPlatePhoto>
    {
        public Guid Id { get; set; }
    }
}