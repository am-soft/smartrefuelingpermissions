﻿using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Responses;
using BusinessLogic.Services.Cameras.Requests;
using Domain.Model;
using MediatR;
using Storage.Repository;

namespace BusinessLogic.Services.Cameras.Handlers
{
    public class TakePhotoHandler : IRequestHandler<TakePhotoRequest, BaseResourceResponse>
    {
        private readonly IRepository<RegistrationPlatePhoto> _photosRepository;

        public TakePhotoHandler(IRepository<RegistrationPlatePhoto> photosRepository)
        {
            _photosRepository = photosRepository;
        }

        public async Task<BaseResourceResponse> Handle(TakePhotoRequest request, CancellationToken cancellationToken)
        {
            var photo = new RegistrationPlatePhoto
            {
                RefuelingId = request.RefuelingId,
                Photo = request.File
            };

            await _photosRepository.Create(photo);

            return new BaseResourceResponse(photo);
        }
    }
}