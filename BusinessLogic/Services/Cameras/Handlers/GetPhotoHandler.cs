﻿using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Cameras.Requests;
using Domain.Model;
using MediatR;
using Storage.Repository;

namespace BusinessLogic.Services.Cameras.Handlers
{
    public class GetPhotoHandler : IRequestHandler<GetPhotoRequest, RegistrationPlatePhoto>
    {
        private readonly IRepository<RegistrationPlatePhoto> _photosRepository;

        public GetPhotoHandler(IRepository<RegistrationPlatePhoto> photosRepository)
        {
            _photosRepository = photosRepository;
        }

        public Task<RegistrationPlatePhoto> Handle(GetPhotoRequest request, CancellationToken cancellationToken)
        {
            var photo = _photosRepository.Get(request.Id);
            return photo;
        }
    }
}