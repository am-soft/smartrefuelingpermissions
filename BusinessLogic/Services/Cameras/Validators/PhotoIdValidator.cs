﻿using System;
using BusinessLogic.Services.Cameras.Requests;
using FluentValidation.Validators;
using MediatR;

namespace BusinessLogic.Services.Cameras.Validators
{
    public class PhotoIdValidator : PropertyValidator
    {
        private readonly IMediator _mediator;

        public PhotoIdValidator(IMediator mediator) : base("Photo does not exist")
        {
            _mediator = mediator;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var id = (Guid)context.PropertyValue;
            var refueling = _mediator.Send(new GetPhotoRequest { Id = id }).Result;
            return refueling != null;
        }
    }
}