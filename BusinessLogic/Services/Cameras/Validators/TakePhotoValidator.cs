﻿using BusinessLogic.Services.Cameras.Requests;
using BusinessLogic.Services.Refuelings.Validators;
using FluentValidation;
using MediatR;

namespace BusinessLogic.Services.Cameras.Validators
{
    public class TakePhotoValidator : AbstractValidator<TakePhotoRequest>
    {
        public TakePhotoValidator(IMediator mediator)
        {
            RuleFor(x => x.RefuelingId)
                .SetValidator(new RefuelingIdValidator(mediator));
        }
    }
}