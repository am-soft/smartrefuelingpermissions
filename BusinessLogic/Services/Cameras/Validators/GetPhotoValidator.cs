﻿using BusinessLogic.Services.Cameras.Requests;
using FluentValidation;
using MediatR;

namespace BusinessLogic.Services.Cameras.Validators
{
    public class GetPhotoValidator : AbstractValidator<GetPhotoRequest>
    {
        public GetPhotoValidator(IMediator mediator)
        {
            RuleFor(x => x.Id)
                .SetValidator(new PhotoIdValidator(mediator));
        }
    }
}