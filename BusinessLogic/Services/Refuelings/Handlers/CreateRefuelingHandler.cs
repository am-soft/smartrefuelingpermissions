﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Responses;
using BusinessLogic.Services.Refuelings.Requests;
using Domain.Model;
using MediatR;
using Storage.Repository;

namespace BusinessLogic.Services.Refuelings.Handlers
{
    public class CreateRefuelingHandler : IRequestHandler<CreateRefuelingRequest, BaseResourceResponse>
    {
        private readonly IRepository<Refueling> _refuelingsRepository;

        public CreateRefuelingHandler(IRepository<Refueling> refuelingsRepository)
        {
            _refuelingsRepository = refuelingsRepository;
        }

        public async Task<BaseResourceResponse> Handle(CreateRefuelingRequest request, CancellationToken cancellationToken)
        {
            var refueling = new Refueling { DateTime = request.DateTime, DistributorId = request.DistributorId };
            await _refuelingsRepository.Create(refueling);

            return new BaseResourceResponse(refueling);
        }
    }
}