﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Refuelings.Requests;
using Domain.Model;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Storage.Repository;

namespace BusinessLogic.Services.Refuelings.Handlers
{
    public class GetRefuelingPhotoToRecognizeHandler : IRequestHandler<GetRefuelingPhotoToRecognizeRequest, RegistrationPlatePhoto>
    {
        private readonly IRepository<RegistrationPlatePhoto> _photosRepository;

        public GetRefuelingPhotoToRecognizeHandler(IRepository<RegistrationPlatePhoto> photosRepository)
        {
            _photosRepository = photosRepository;
        }

        public async Task<RegistrationPlatePhoto> Handle(GetRefuelingPhotoToRecognizeRequest request, CancellationToken cancellationToken)
        {
            var query = _photosRepository.Query()
                .Where(x => x.RefuelingId == request.RefuelingId)
                .OrderByDescending(x => x.DateTime);

            var photo = await query.FirstOrDefaultAsync(cancellationToken: cancellationToken);

            return photo;
        }
    }
}