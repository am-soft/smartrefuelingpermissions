﻿using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Refuelings.Requests;
using Domain.Model;
using MediatR;
using Storage.Repository;

namespace BusinessLogic.Services.Refuelings.Handlers
{
    public class GetRefuelingHandler : IRequestHandler<GetRefuelingRequest, Refueling>
    {
        private readonly IRepository<Refueling> _refuelingsRepository;

        public GetRefuelingHandler(IRepository<Refueling> refuelingsRepository)
        {
            _refuelingsRepository = refuelingsRepository;
        }

        public async Task<Refueling> Handle(GetRefuelingRequest request, CancellationToken cancellationToken)
        {
            return await _refuelingsRepository.Get(request.Id);
        }
    }
}