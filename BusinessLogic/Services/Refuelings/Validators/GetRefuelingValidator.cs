﻿using BusinessLogic.Services.Refuelings.Requests;
using FluentValidation;
using MediatR;

namespace BusinessLogic.Services.Refuelings.Validators
{
    public class GetRefuelingValidator : AbstractValidator<GetRefuelingRequest>
    {
        public GetRefuelingValidator(IMediator mediator)
        {
            RuleFor(x => x.Id)
                .SetValidator(new RefuelingIdValidator(mediator));
        }
    }
}