﻿using System;
using BusinessLogic.Services.Refuelings.Requests;
using FluentValidation.Validators;
using MediatR;

namespace BusinessLogic.Services.Refuelings.Validators
{
    public class RefuelingIdValidator : PropertyValidator
    {
        private readonly IMediator _mediator;

        public RefuelingIdValidator(IMediator mediator) : base("Refueling does not exist")
        {
            _mediator = mediator;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            var id = (Guid)context.PropertyValue;
            var refueling = _mediator.Send(new GetRefuelingRequest { Id = id }).Result;
            return refueling != null;
        }
    }
}