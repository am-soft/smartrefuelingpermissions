﻿using System;
using BusinessLogic.Services.Refuelings.Requests;
using FluentValidation;

namespace BusinessLogic.Services.Refuelings.Validators
{
    public class CreateRefuelingValidator : AbstractValidator<CreateRefuelingRequest>
    {
        public CreateRefuelingValidator()
        {
            RuleFor(x => x.DistributorId)
                .NotEmpty();

            RuleFor(x => x.DateTime)
                .NotEmpty()
                .GreaterThan(DateTime.MinValue)
                .LessThan(DateTime.MaxValue);
        }
    }
}