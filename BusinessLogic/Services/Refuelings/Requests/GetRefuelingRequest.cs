﻿using System;
using Domain.Model;
using MediatR;

namespace BusinessLogic.Services.Refuelings.Requests
{
    public class GetRefuelingRequest : IRequest<Refueling>
    {
        public Guid Id { get; set; }
    }
}