﻿using System;
using Domain.Model;
using MediatR;

namespace BusinessLogic.Services.Refuelings.Requests
{
    public class GetRefuelingPhotoToRecognizeRequest : IRequest<RegistrationPlatePhoto>
    {
        public Guid RefuelingId { get; set; }
    }
}