﻿using System;
using BusinessLogic.Responses;
using MediatR;

namespace BusinessLogic.Services.Refuelings.Requests
{
    public class CreateRefuelingRequest : IRequest<BaseResourceResponse>
    {
        public Guid DistributorId { get; set; }
        public DateTime DateTime { get; set; }
    }
}