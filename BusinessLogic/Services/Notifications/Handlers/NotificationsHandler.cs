﻿using System.Threading;
using System.Threading.Tasks;
using BusinessLogic.Services.Notifications.Requests;
using MediatR;

namespace BusinessLogic.Services.Notifications.Handlers
{
    public class NotificationsHandler :
        IRequestHandler<NotifyUnableToRecognizeRequest>,
        IRequestHandler<NotifyLicensePlateNotAllowedRequest>
    {
        public Task<Unit> Handle(NotifyUnableToRecognizeRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(Unit.Value);
        }

        public Task<Unit> Handle(NotifyLicensePlateNotAllowedRequest request, CancellationToken cancellationToken)
        {
            return Task.FromResult(Unit.Value);
        }
    }
}