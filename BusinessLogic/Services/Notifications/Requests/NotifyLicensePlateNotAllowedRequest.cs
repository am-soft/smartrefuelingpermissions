﻿using Domain.Model;
using MediatR;

namespace BusinessLogic.Services.Notifications.Requests
{
    public class NotifyLicensePlateNotAllowedRequest : IRequest
    {
        public Refueling Refueling { get; set; }
    }
}