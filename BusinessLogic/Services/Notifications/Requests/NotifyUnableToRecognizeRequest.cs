﻿using Domain.Model;
using MediatR;

namespace BusinessLogic.Services.Notifications.Requests
{
    public class NotifyUnableToRecognizeRequest : IRequest
    {
        public Refueling Refueling { get; set; }
    }
}