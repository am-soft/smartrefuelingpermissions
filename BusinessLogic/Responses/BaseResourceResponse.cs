﻿using System;
using BusinessLogic.Services.ResourceUrl;
using Domain.Model;

namespace BusinessLogic.Responses
{
    public class BaseResourceResponse
    {
        public BaseResourceResponse(BaseEntity resource)
        {
            Id = resource.Id;
            ResourceUrl = ResourceUrlService.GetUrl(resource);
        }

        public Guid Id { get; set; }
        public string ResourceUrl { get; set; }
    }
}