﻿using System;
using System.Threading.Tasks;
using BusinessLogic.Services.Verifications.Requests;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class VerifyController : BaseController
    {
        public VerifyController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        [HttpPost("/verify")]
        public async Task<IActionResult> VerifyPermissions(VerifyPermissionsRequest request) => await ProcessAsync(request);
    }
}