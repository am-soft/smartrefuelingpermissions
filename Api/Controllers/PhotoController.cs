﻿using System;
using System.IO;
using System.Threading.Tasks;
using BusinessLogic.Services.Cameras.Requests;
using BusinessLogic.Services.Cameras.Validators;
using Domain.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// This controllers handles photo-related operations. Use those endpoints for taking and getting refuelings photos.
    /// </summary>
    public class PhotoController : BaseController
    {
        public PhotoController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        /// <summary>
        /// This endpoint returns the photo with given Id.
        /// </summary>
        /// <param name="id">RegistrationPlatePhoto Id.</param>
        /// <seealso cref="TakePhoto(Guid, IFormFile)"/>
        /// <returns>Saved refueling photo</returns>
        [HttpGet("/photo/{id}")]
        public async Task<IActionResult> GetPhoto(Guid id) =>
            await ValidateAndProcessAsync<GetPhotoValidator, RegistrationPlatePhoto>(new GetPhotoRequest { Id = id });

        /// <summary>
        /// This endpoint takes the photo by capturing the frame from camera assigned to refueling distributor.
        /// </summary>
        /// <param name="refuelingId">Id of previously created refueling. Refueling contains information about distributor.</param>
        /// <param name="photo">For testing purposes, photo can be uploaded as a form field.</param>
        /// <returns>Returns Id of uploaded photo and resource URL pointing to it.</returns>
        /// <seealso cref="GetPhoto(Guid)"/>
        [HttpPost("/photo/take/{refuelingId}")]
        public async Task<IActionResult> TakePhoto(Guid refuelingId, IFormFile photo)
        {
            await using var stream = photo.OpenReadStream();
            using var reader = new BinaryReader(stream);
            var bytes = reader.ReadBytes((int)photo.Length);

            var request = new TakePhotoRequest
            {
                RefuelingId = refuelingId,
                File = bytes
            };

            return await ProcessAsync(request);
        }
    }
}