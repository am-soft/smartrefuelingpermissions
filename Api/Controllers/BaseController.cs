﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IServiceProvider _serviceProvider;

        public BaseController(IServiceProvider serviceProvider)
        {
            _mediator = (IMediator)serviceProvider.GetService(typeof(IMediator));
            _serviceProvider = serviceProvider;
        }

        protected async Task<IActionResult> ValidateAndProcessAsync<TValidator, TResponse>(IRequest<TResponse> request)
        {
            var validator = (IValidator) _serviceProvider.GetService(typeof(TValidator));
            var result = await validator.ValidateAsync(request);
            result.AddToModelState(ModelState, "");
            return await ProcessAsync(request);
        }

        protected async Task<IActionResult> ProcessAsync<TResponse>(IRequest<TResponse> request)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var result = await _mediator.Send(request);
                return Ok(result);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}