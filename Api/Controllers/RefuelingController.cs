﻿using System;
using System.Threading.Tasks;
using BusinessLogic.Services.Refuelings.Requests;
using BusinessLogic.Services.Refuelings.Validators;
using Domain.Model;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// This controller handles the creation and storage of refuelings.
    /// </summary>
    public class RefuelingController : BaseController
    {
        public RefuelingController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        /// <summary>
        /// Creates the refueling assigned to given distributor id.
        /// </summary>
        /// <param name="request">Contains information about refueling (distributor, time, etc.).</param>
        /// <returns>Id and resource URL pointing to created refueling.</returns>
        /// <seealso cref="GetRefueling(Guid)"/>
        [HttpPost("/refueling")]
        public async Task<IActionResult> CreateRefueling(CreateRefuelingRequest request) =>
            await ProcessAsync(request);

        /// <summary>
        /// Returns the refueling for given id.
        /// </summary>
        /// <param name="id">Id of refueling</param>
        /// <returns>Refueling</returns>
        /// <seealso cref="CreateRefueling(CreateRefuelingRequest)"/>
        [HttpGet("/refueling/{id}")]
        public async Task<IActionResult> GetRefueling(Guid id) =>
            await ValidateAndProcessAsync<GetRefuelingValidator, Refueling>(new GetRefuelingRequest { Id = id });
    }
}
