﻿using System;
using System.Threading.Tasks;
using BusinessLogic.Services.Recognitions.Requests;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    /// <summary>
    /// This controller handles license plate recognition from photo.
    /// </summary>
    public class RecognitionsController : BaseController
    {
        public RecognitionsController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        /// <summary>
        /// Recognize license plate of vehicle of given refueling.
        /// </summary>
        /// <param name="request">RecognizeRequest contains refueling parameters for which recognition should be performed</param>
        /// <returns>Recognition result containing information about recognized license plate number</returns>
        [HttpPost("/recognitions/make")]
        public async Task<IActionResult> Recognize(RecognizeRequest request) =>
            await ProcessAsync(request);
    }
}